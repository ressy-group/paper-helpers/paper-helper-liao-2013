#!/usr/bin/env python

"""
Make CSV of antibody seq attributes from downloaded GenBank files.
"""

import re
import sys
from csv import DictReader, DictWriter
from Bio import SeqIO

def parse_seq_desc(txt):
    match = re.match(r"([A-Z0-9.]+) Homo sapiens isolate ([A-Za-z0-9_]+) immunoglobulin (heavy|light) chain variable region( \(IGHV\))? mRNA, partial cds", txt)
    grps = match.groups()
    keys = ["Accession", "SeqID", "Chain", "IGHV"]
    fields = {k: v for k, v in zip(keys, grps)}
    # Not sure what this signifies, actually.  They're all full length and all
    # heavy chain seqs get this "(IGHV)" thing added.
    del fields["IGHV"]
    match = re.match(r"([A-UW-Z0-9]+)_?(V[HKL])?(_UA)?", fields["SeqID"])
    grps = match.groups()
    fields["Antibody"] = grps[0]
    fields["Locus"] = {"VH": "IGH", "VL": "IGL", "VK": "IGK"}[grps[1].removeprefix("_")]
    fields["Category"] = "UA" if "_UA" == grps[2] else "mAb"
    # There's a mistake for this one, looks like; it says heavy chain but the
    # seq ID implies light chain and IgBLAST agrees
    if fields["Accession"] == "KC417410.1":
        fields["Chain"] = "light"
    return fields

FIELDS = [
    "SeqID",
    "Antibody",
    "Category",
    "Locus",
    "Chain",
    "Accession",
    "Seq"]

def make_seqs_sheet(fastas):
    all_attrs = []
    for fasta in fastas:
        with open(fasta) as f_in:
            for record in SeqIO.parse(f_in, "fasta"):
                fields = parse_seq_desc(record.description)
                fields["Seq"] = str(record.seq)
                all_attrs.append(fields)
    writer = DictWriter(sys.stdout, FIELDS, lineterminator="\n")
    writer.writeheader()
    writer.writerows(all_attrs)

if __name__ == "__main__":
    make_seqs_sheet(sys.argv[1:])
