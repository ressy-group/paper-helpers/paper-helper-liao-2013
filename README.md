# Data Gathering from Liao 2013

Some sequences and metadata from:

Hua-Xin Liao et al.
Vaccine Induction of Antibodies against a Structurally Heterogeneous Site of Immune Pressure within HIV-1 Envelope Protein Variable Regions 1 and 2.
Immunity, Volume 38, Issue 1, 176-186
<https://doi.org/10.1016/j.immuni.2012.11.011>

 * Antibody sequence GenBank entries: KC417393 - KC417414 (apparent typo for
   KC417410; appears to be light, not heavy)
 * PDB entries:
   * CH58: 4HQQ
   * CH58+V2: 4HPO
   * CH59+V2: 4HPY
